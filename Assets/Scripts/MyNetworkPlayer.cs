﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using TMPro;

public class MyNetworkPlayer : NetworkBehaviour
{

    [SerializeField] private TMP_Text displayNameText = null;
    [SerializeField] private Renderer displayColourRenderer = null;

    
    [SyncVar(hook="HandleDisplayColourUpdated")]
    [SerializeField]
    private Color displayColour = Color.black;

    [SyncVar(hook="HandleDisplayNameUpdated")]
    [SerializeField]
    private string displayName = "Missing Name";

    [Server]
    public void SetDisplayName(string newDisplayName)
    {
        displayName=newDisplayName;
    }

    [Server]
    public void SetDisplayColour(Color newDisplayColour)
    {
        displayColour=newDisplayColour;
    }

    private void HandleDisplayColourUpdated(Color oldColour, Color newColour)
    {
        displayColourRenderer.material.SetColor("_BaseColor", newColour);
    }

    private void HandleDisplayNameUpdated( string oldName, string newName)
    {
        displayNameText.text = newName;
    }

}
