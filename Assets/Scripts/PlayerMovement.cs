﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using UnityEngine.AI;

public class PlayerMovement : NetworkBehaviour
{

    [SerializeField] private NavMeshAgent agent = null;
    private Camera mainCamera;

    public override void OnStartAuthority()
    {
        mainCamera=Camera.main;
    }

    [ClientCallback]
    private void Update()
    {
       if (isLocalPlayer)
       {
        
       agent.Move(new Vector3(Input.GetAxis("Horizontal"),0, Input.GetAxis("Vertical")) * 4 * Time.fixedDeltaTime);

       }
    }




    
}
